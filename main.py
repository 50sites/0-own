#! usr/bin/env python3

import socket


class Headers():
    def __init__(self):
        self.http = 'HTTP/1.1'
        self.codes = {405: 'Method not allowed',
                              404: 'Non found',
                              200: '200 OK',
                             }

    def gen(self, method, url):
        if not method == 'GET':
            return self.http + self.codes[405]
        return self.http + self.codes[200]





def run():
    sock = ('localhost', 5000)
    server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, True)
    server_socket.bind(sock)
    server_socket.listen()
    print(sock, 'listen....')

    while True:
        client_socket, addr = server_socket.accept()
        request = client_socket.recv(1024)
        #print(request, "\n", addr)
        request_list = request.decode('utf-8').split()
        method, url = request_list[0:2]
        print(url)

        response = Headers().gen(method, url)
        print(response)
        print('{} {}'.format(response, 'Hello').encode())
        client_socket.sendall('{}\n\n {}'.format(response, 'Hello').encode())
        client_socket.close()



if __name__ == '__main__':
    run()